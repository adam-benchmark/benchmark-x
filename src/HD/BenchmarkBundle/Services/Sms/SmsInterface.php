<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-07
 * Time: 19:38
 */

namespace HD\BenchmarkBundle\Services\Sms;

/**
 * Interface SmsInterface
 * @package HD\BenchmarkBundle\Services\Sms
 */
interface SmsInterface
{
    public function sendSms();
}