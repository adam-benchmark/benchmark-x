<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-14
 * Time: 08:03
 */

namespace HD\BenchmarkBundle\Services\LogService;

use HD\BenchmarkBundle\Services\DTO\Benchmark\DTOShowResult;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LogService implements LogServiceInterface
{
    public function appendInfoToLogger(DTOShowResult $dtoShowResult, $fileName)
    {
        $pathLogger = '../var/logs/logs-benchmark/' . $fileName . '.log';
        $logger = new Logger('benchmark-logger');
        $logger->pushHandler(new StreamHandler($pathLogger), Logger::INFO);
        $logger->info('calculate-loading-time', [
            'dtoShowResult' => $dtoShowResult
        ]);
    }
}