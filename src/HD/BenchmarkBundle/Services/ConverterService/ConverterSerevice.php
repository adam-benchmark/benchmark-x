<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-15
 * Time: 20:21
 */

namespace HD\BenchmarkBundle\Services\ConverterService;

use HD\BenchmarkBundle\Services\Benchmark\BenchmarkService;
use HD\BenchmarkBundle\Services\DTO\Benchmark\DTOBenchmark;
use HD\BenchmarkBundle\Services\DTO\Benchmark\DTOShowResult;

class ConverterSerevice
{
    /**
     * @var BenchmarkService
     */
    private $benchmarkService;

    /**
     * ConverterSerevice constructor.
     * @param BenchmarkService $benchmarkService
     */
    public function __construct(BenchmarkService $benchmarkService)
    {
        $this->benchmarkService = $benchmarkService;
    }

    /**
     * @param DTOBenchmark $dtoBenchmark
     * @return DTOShowResult
     */
    public function convertFormDataToDto(DTOBenchmark $dtoBenchmark)
    {
        $dtoShowResult = new DTOShowResult();
        $dtoShowResult->timeFirst = $this->benchmarkService->calculateLoadingTime($dtoBenchmark->urlFirst);
        $dtoShowResult->timeSecond = $this->benchmarkService->calculateLoadingTime($dtoBenchmark->urlSecond);
        $dtoShowResult->difference = $dtoShowResult->timeFirst - $dtoShowResult->timeSecond;
        $dtoShowResult->comment = $dtoBenchmark->comment;
        $dtoShowResult->dateTime = new \DateTime();

        return $dtoShowResult;
    }
}