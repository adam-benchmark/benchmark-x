<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-15
 * Time: 21:52
 */

namespace HD\BenchmarkBundle\Services\MessageCoordinator;


use SystemBundle\Model\MailQueue;
use SystemBundle\Model\MailQueueQuery;

class NotifierMessageEmail implements NotifierMessageInterface
{

    public function sendMessage()
    {
        $mail = new MailQueue();
        $mail->setContent('this website is slow');
        $mail->setCreatedAt(new \DateTime());
        $mail->setTitle('Notice about Your website');
        $mail->save();

        return $mail;
    }
}