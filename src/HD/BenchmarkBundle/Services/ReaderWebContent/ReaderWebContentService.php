<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-07
 * Time: 16:23
 */

namespace HD\BenchmarkBundle\Services\ReaderWebContent;


use Exception;

class ReaderWebContentService
{
    /**
     * @param $url
     * @return bool|string
     */
    public function getContents($url)
    {
        try {
            $content = file_get_contents($url);
        }catch (Exception $exception) {
            $content = null;
        }

        return $content;
    }
}