<?php

namespace HD\BenchmarkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('HDBenchmarkBundle:Default:index.html.twig');
    }
}
