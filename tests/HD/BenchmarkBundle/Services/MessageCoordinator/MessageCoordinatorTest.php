<?php

namespace HD\BenchmarkBundle\Services\MessageCoordinator;


use Exception;
use HD\BenchmarkBundle\Services\Mailer\DTOMailer;
use HD\BenchmarkBundle\Services\Mailer\MailerInterface;
use HD\BenchmarkBundle\Services\Mailer\MailerService;
use HD\BenchmarkBundle\Services\Sms\SmsInterface;
use HD\BenchmarkBundle\Services\Sms\SmsService;
use Tests\PrivateAccessTestCase;

class MessageCoordinatorTest extends PrivateAccessTestCase
{
    /** @var MailerInterface  */
    private $mailerInterface;
    /** @var SmsInterface */
    private $smsInterface;
    /** @var  MessageCoordinatorFactory */
    private $messageCoordinator;
    /** @var  \Mockery */
    private $dtoMailer;

    public function setUp()
    {
        $this->mailerInterface = $this->createMock(MailerInterface::class);
        $this->smsInterface = $this->createMock(SmsInterface::class);
        $this->messageCoordinator = new MessageCoordinatorFactory($this->mailerInterface, $this->smsInterface);
        $this->dtoMailer = $this->createMock(DTOMailer::class);
    }

    public function testSendMail()
    {
        $result = $this->messageCoordinator->sendMail($this->dtoMailer);

        $this->assertEquals(null, $result);
    }

    public function testSendSms()
    {
        $result = $this->messageCoordinator->sendSms();

        $this->assertEquals(true, $result);
    }

    public function testSendMailException()
    {
        $mailService = $this->createMock(MailerService::class);
        $smsService = $this->createMock(SmsService::class);
        $messageCoordinator = new MessageCoordinatorFactory($mailService, $smsService);

        $mailService
            ->method('sendMail')
            ->with([$this->dtoMailer])
            ->will($this->throwException(new Exception));

        $result = $messageCoordinator->sendMail($this->dtoMailer);
        $this->assertFalse($result);
    }
}
