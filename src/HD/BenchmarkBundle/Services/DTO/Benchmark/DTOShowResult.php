<?php

namespace HD\BenchmarkBundle\Services\DTO\Benchmark;


class DTOShowResult
{
    public $timeFirst;
    public $timeSecond;
    public $difference;
    public $comment;
    public $dateTime;
}