<?php

namespace HD\BenchmarkBundle\Controller\Benchmark;

use HD\BenchmarkBundle\Form\FormBenchmarkType;
use HD\BenchmarkBundle\Services\DTO\Benchmark\DTOBenchmark;
use HD\BenchmarkBundle\Services\DTO\Benchmark\DTOShowResult;
use HD\BenchmarkBundle\Services\InformationResolverService\InformationResolverService;
use HD\BenchmarkBundle\Utils\EmailPreset\EmailPreset;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use SystemBundle\Model\UserQuery;

class BenchmarkController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $dataBenchmark = new DTOBenchmark();
        $dtoShowResult = new DTOShowResult();

        $form = $this->createForm(FormBenchmarkType::class, $dataBenchmark);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var DTOShowResult $dtoShowResult */
            $dtoShowResult = $this->convertFormDataToDto($form->getData());
            $this->appendInfoToLogger($dtoShowResult, 'benchmark');
            $this->checkIfSendMessage($dtoShowResult);
        }

        return $this->render('@HDBenchmark/Benchmark/benchmark.html.twig', [
            'title' => 'benchmark Your websites',
            'form' => $form->createView(),
            'showResult' => $dtoShowResult ? $dtoShowResult : new DTOShowResult(),
        ]);
    }

    /**
     * @param DTOShowResult $dtoShowResult
     */
    private function checkIfSendMessage(DTOShowResult $dtoShowResult)
    {
        /** @var  EmailPreset $emailPreset */
        $emailPreset = $this->buildMessagePreset();

        $result = $this->get('hd.CheckerIfSendMessage')->checkIfSendMessage($dtoShowResult);
    }

    /**
     * @param DTOShowResult $dtoShowResult
     * @param string $fileName
     */
    private function appendInfoToLogger(DTOShowResult $dtoShowResult, $fileName)
    {
        $this->get('hd.log.service')->appendInfoToLogger($dtoShowResult, $fileName);
    }

    /**
     * @param DTOBenchmark $dtoBenchmark
     * @return DTOShowResult
     */
    private function convertFormDataToDto(DTOBenchmark $dtoBenchmark)
    {
        return $this->get('hd.converter.convertFormDataToDto')->convertFormDataToDto($dtoBenchmark);
    }

    /**
     * @return EmailPreset
     */
    private function buildMessagePreset()
    {
        $emailPreset = new EmailPreset();
        $emailPreset->setSubject('This could be interesting for You');
        $emailPreset->setReplayTo($this->container->getParameter('benchmark_replay_to'));
        $emailPreset->setFrom($this->container->getParameter('benchmark_mail_from'));
        $emailPreset->setTo($this->container->getParameter('benchmark_mail_to'));
        $emailPreset->setBcc($this->container->getParameter('benchmark_mail_bcc'));
        $emailPreset->setEmail('email.client@example.com');

        return $emailPreset;
    }

    /**
     * @param DTOShowResult $dtoShowResult
     */
    private function sendEmailResolve(DTOShowResult $dtoShowResult)
    {
        /** @var  EmailPreset $emailPreset */
        $emailPreset = $this->buildMessagePreset();

        $informationResolverService = $this->get('hd.information_resolver_service');
        $result = $informationResolverService->checkIfSendEmail($emailPreset, $dtoShowResult);
        $this->showFlashForEmail($result);
    }

    /**
     * @param DTOShowResult $dtoShowResult
     */
    private function sendSmsResolve(DTOShowResult $dtoShowResult)
    {
        $informationResolverService = $this->get('hd.information_resolver_service');
        $informationResolverService->checkIfSentSms($dtoShowResult);
    }

    /**
     * @param integer $messageNumber
     */
    private function showFlashForEmail($messageNumber)
    {
        if ($messageNumber == InformationResolverService::STATUS_EMAIL_OK) {
            $this->addFlash('success', 'Email has been sent correctly');
        } else {
            $this->addFlash('error', 'Problem with sending email!');
        }
    }


}
