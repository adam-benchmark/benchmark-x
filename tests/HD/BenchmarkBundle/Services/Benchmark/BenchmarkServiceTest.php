<?php

namespace HD\BenchmarkBundle\Services\Benchmark;

use HD\BenchmarkBundle\Services\ReaderWebContent\ReaderWebContentService;
use Mockery_MockTest;
use Tests\PrivateAccessTestCase;


class BenchmarkServiceTest extends PrivateAccessTestCase
{
    /** @var  Mockery_MockTest */
    private $benchmarkService;
    /** @var Mockery_MockTest */
    private $readerWebContentService;
    public function setUp()
    {
        $this->readerWebContentService = $this->createMock(ReaderWebContentService::class);
        $this->benchmarkService = new BenchmarkService($this->readerWebContentService);
    }
    public function testCalculateLoadingTimeIsZero()
    {
        $urlTest = 'abc';
        $result = $this->benchmarkService->calculateLoadingTime($urlTest);
        $this->assertEquals(0, $result);
    }

    public function testCalculateLoadingTimeReturnFloat()
    {
        $urlTest = 'url';
        $contentTest = 'contentTest';

        $this->readerWebContentService
            ->expects($this->once())
            ->method('getContents')
            ->with($urlTest)
            ->willReturn($contentTest);

        $result = $this->benchmarkService->calculateLoadingTime($urlTest);
        $this->assertInternalType('float', $result);
    }
}
