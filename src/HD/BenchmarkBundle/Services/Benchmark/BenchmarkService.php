<?php

namespace HD\BenchmarkBundle\Services\Benchmark;


use HD\BenchmarkBundle\Services\ReaderWebContent\ReaderWebContentService;

class BenchmarkService
{
    /**
     * @var ReaderWebContentService
     */
    private $readerWebContentService;

    /**
     * BenchmarkService constructor.
     * @param ReaderWebContentService $readerWebContentService
     */
    public function __construct(ReaderWebContentService $readerWebContentService)
    {
        $this->readerWebContentService = $readerWebContentService;
    }

    /**
     * @param $url
     * @return int|mixed
     */
    public function calculateLoadingTime($url)
    {
        $time_start = microtime(true);

        if(!empty($this->readerWebContentService->getContents($url))){
            $time_end = microtime(true);
            $time = $time_end - $time_start;
            return $time;
        }

        return 0;
    }


}