<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-15
 * Time: 20:26
 */

namespace HD\BenchmarkBundle\Services\InformationResolverService;


use HD\BenchmarkBundle\Services\DTO\Benchmark\DTOShowResult;
use HD\BenchmarkBundle\Services\Mailer\DTOMailer;
use HD\BenchmarkBundle\Services\MessageCoordinator\MessageCoordinatorFactory;
use HD\BenchmarkBundle\Utils\EmailPreset\EmailPreset;

class InformationResolverService
{
    const STATUS_EMAIL_ERROR = 1;
    const STATUS_EMAIL_OK = 2;

    /** @var MessageCoordinatorFactory */
    private $messageCoordinator;

    /** @var DTOShowResult */
    private $dtoShowResult;

    /**
     * InformationResolverService constructor.
     * @param MessageCoordinatorFactory $messageCoordinator
     */
    public function __construct(MessageCoordinatorFactory $messageCoordinator)
    {
        $this->messageCoordinator = $messageCoordinator;
    }

    /**
     * @param EmailPreset $emailPreset
     * @param DTOShowResult $dtoShowResult
     * @return int
     */
    public function checkIfSendEmail(EmailPreset $emailPreset, DTOShowResult $dtoShowResult)
    {
        if ($dtoShowResult->difference > 0) {
            if ($this->messageCoordinator->sendMail($emailPreset) > 0) {
                /*$this->addFlash('success', 'Email has been sent correctly');*/

                return self::STATUS_EMAIL_OK;
            } else {
                /*$this->addFlash('error', 'Problem with sending email!');*/

                return self::STATUS_EMAIL_ERROR;
            }
        }

        return self::STATUS_EMAIL_ERROR;
    }

    public function checkIfSentSms(DTOShowResult $dtoShowResult)
    {
        if ($dtoShowResult->timeFirst * 2 <= $dtoShowResult->timeSecond) {
            $this->messageCoordinator->sendSms();
        }
    }
}