<?php

namespace SystemBundle\Model;

use SystemBundle\Model\Base\PropertyDetailsQuery as BasePropertyDetailsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'property_details' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PropertyDetailsQuery extends BasePropertyDetailsQuery
{

}
