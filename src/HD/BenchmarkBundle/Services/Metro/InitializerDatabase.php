<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-11-06
 * Time: 13:25
 */

namespace HD\BenchmarkBundle\Services\Metro;


use HD\BenchmarkBundle\Services\DTO\Property\DTOOptionInitial;

class InitializerDatabase
{
    /**
     * @param DTOOptionInitial $option
     */
    public function initialDatabase(DTOOptionInitial $option)
    {
        $this->writeRandomToDatabase();
    }

    private function writeRandomToDatabase()
    {

    }

    /**
     * @param DTOOptionInitial $option
     * @return int
     */
    private function chooseBlock(DTOOptionInitial $option)
    {
        $block = 0;
        switch ($option) {
            case 1:
                $block = 1;
                break;

            case 2:
                $block = 2;
                break;

            default:
                $block = 0;
                break;
        }

        return $block;
    }
}