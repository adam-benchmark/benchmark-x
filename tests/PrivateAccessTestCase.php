<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-07
 * Time: 15:43
 */

namespace Tests;
use PHPUnit_Framework_TestCase;


/**
 * Class PrivateAccessTestCase
 * @package Tests
 *
 * By extending this class instead of extending directly \PHPUnit_Framework_TestCase
 * we will have extra tools to execute private methods of tested object (which isn't Mock)
 */
class PrivateAccessTestCase extends PHPUnit_Framework_TestCase
{
    /**
     * Executes private or protected method of object and provide arguments
     *
     * @param mixed $object
     * @param string $methodName
     * @param array $arguments
     * @return mixed
     */
    public static function executePrivateMethod($object, $methodName, $arguments = []) {
        $method = self::getPrivateMethodOfClass($methodName, get_class($object));
        return $method->invokeArgs($object, $arguments);
    }
    /**
     * Gets value of private or protected property
     *
     * @param mixed $object
     * @param $propertyName
     * @return mixed
     */
    public static function getPrivatePropertyValue($object, $propertyName) {
        $property = self::getPrivatePropertyOfClass($propertyName, get_class($object));
        return $property->getValue($object);
    }
    /**
     * Sets value of private or protected property
     *
     * @param mixed $object
     * @param string $propertyName
     * @param mixed $value
     * @return mixed
     */
    public static function setPrivatePropertyValue($object, $propertyName, $value) {
        $property = self::getPrivatePropertyOfClass($propertyName, get_class($object));
        return $property->setValue($object, $value);
    }
    /**
     * Method allow us to get access to protected or private method
     *
     * @param string $methodName
     * @param string $className
     * @return mixed
     */
    private static function getPrivateMethodOfClass($methodName, $className) {
        $class = new \ReflectionClass($className);
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        return $method;
    }
    /**
     * Method allow us to get access to protected or private Property
     *
     * @param string $propertyName
     * @param string $className
     * @return mixed
     */
    private static function getPrivatePropertyOfClass($propertyName, $className) {
        $class = new \ReflectionClass($className);
        $property = $class->getProperty($propertyName);
        $property->setAccessible(true);
        return $property;
    }
}