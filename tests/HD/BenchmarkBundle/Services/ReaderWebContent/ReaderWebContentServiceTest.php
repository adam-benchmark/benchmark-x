<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-07
 * Time: 16:44
 */

namespace HD\BenchmarkBundle\Services\ReaderWebContent;

use Tests\PrivateAccessTestCase;

class ReaderWebContentServiceTest extends PrivateAccessTestCase
{
    /** @var  \Mockery_MockTest */
    private $readerWebContentService;

    public function testGetContents()
    {
        $urlTest = 'url';
        $contentTest = 'abc';
        /*$this->readerWebContentService = $this->createMock(ReaderWebContentService::class);
        $this->readerWebContentService
            ->method('getContents')
            ->with($urlTest)
            ->willReturn($contentTest);*/

        $readerWebContentService = new ReaderWebContentService();
        $result = $readerWebContentService->getContents($urlTest);
        $this->assertEquals(null, $result);
    }
}
