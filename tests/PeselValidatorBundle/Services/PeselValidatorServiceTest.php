<?php

namespace PeselValidatorBundle\Services;

use Tests\PrivateAccessTestCase;

class PeselValidatorServiceTest extends PrivateAccessTestCase
{
    /** @var  PeselValidatorService */
    private $peselValidatorService;

    public function setUp()
    {
        $this->peselValidatorService = new PeselValidatorService();
    }

    public function testValidateChecksum()
    {
        $pesel = '78122800992';
        $result = $this->executePrivateMethod($this->peselValidatorService, 'validateChecksum', [$pesel]);
        $this->assertNull($result);
    }
}
