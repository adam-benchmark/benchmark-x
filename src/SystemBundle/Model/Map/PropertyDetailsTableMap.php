<?php

namespace SystemBundle\Model\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use SystemBundle\Model\PropertyDetails;
use SystemBundle\Model\PropertyDetailsQuery;


/**
 * This class defines the structure of the 'property_details' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PropertyDetailsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\SystemBundle.Model.Map.PropertyDetailsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'property_details';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\SystemBundle\\Model\\PropertyDetails';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\SystemBundle.Model.PropertyDetails';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the id field
     */
    const COL_ID = 'property_details.id';

    /**
     * the column name for the property_id field
     */
    const COL_PROPERTY_ID = 'property_details.property_id';

    /**
     * the column name for the size field
     */
    const COL_SIZE = 'property_details.size';

    /**
     * the column name for the number_of_room field
     */
    const COL_NUMBER_OF_ROOM = 'property_details.number_of_room';

    /**
     * the column name for the number_of_floors field
     */
    const COL_NUMBER_OF_FLOORS = 'property_details.number_of_floors';

    /**
     * the column name for the location field
     */
    const COL_LOCATION = 'property_details.location';

    /**
     * the column name for the price field
     */
    const COL_PRICE = 'property_details.price';

    /**
     * the column name for the street_id field
     */
    const COL_STREET_ID = 'property_details.street_id';

    /**
     * the column name for the house_number field
     */
    const COL_HOUSE_NUMBER = 'property_details.house_number';

    /**
     * the column name for the gps_longitude field
     */
    const COL_GPS_LONGITUDE = 'property_details.gps_longitude';

    /**
     * the column name for the gps_latitude field
     */
    const COL_GPS_LATITUDE = 'property_details.gps_latitude';

    /**
     * the column name for the flat_number field
     */
    const COL_FLAT_NUMBER = 'property_details.flat_number';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'property_details.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'property_details.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'PropertyDetailsId', 'Size', 'NumberOfRoom', 'NumberOfFloors', 'Location', 'Price', 'StreetId', 'HouseNumber', 'GpsLongitude', 'GpsLatitude', 'FlatNumber', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'propertyDetailsId', 'size', 'numberOfRoom', 'numberOfFloors', 'location', 'price', 'streetId', 'houseNumber', 'gpsLongitude', 'gpsLatitude', 'flatNumber', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(PropertyDetailsTableMap::COL_ID, PropertyDetailsTableMap::COL_PROPERTY_ID, PropertyDetailsTableMap::COL_SIZE, PropertyDetailsTableMap::COL_NUMBER_OF_ROOM, PropertyDetailsTableMap::COL_NUMBER_OF_FLOORS, PropertyDetailsTableMap::COL_LOCATION, PropertyDetailsTableMap::COL_PRICE, PropertyDetailsTableMap::COL_STREET_ID, PropertyDetailsTableMap::COL_HOUSE_NUMBER, PropertyDetailsTableMap::COL_GPS_LONGITUDE, PropertyDetailsTableMap::COL_GPS_LATITUDE, PropertyDetailsTableMap::COL_FLAT_NUMBER, PropertyDetailsTableMap::COL_CREATED_AT, PropertyDetailsTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'property_id', 'size', 'number_of_room', 'number_of_floors', 'location', 'price', 'street_id', 'house_number', 'gps_longitude', 'gps_latitude', 'flat_number', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'PropertyDetailsId' => 1, 'Size' => 2, 'NumberOfRoom' => 3, 'NumberOfFloors' => 4, 'Location' => 5, 'Price' => 6, 'StreetId' => 7, 'HouseNumber' => 8, 'GpsLongitude' => 9, 'GpsLatitude' => 10, 'FlatNumber' => 11, 'CreatedAt' => 12, 'UpdatedAt' => 13, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'propertyDetailsId' => 1, 'size' => 2, 'numberOfRoom' => 3, 'numberOfFloors' => 4, 'location' => 5, 'price' => 6, 'streetId' => 7, 'houseNumber' => 8, 'gpsLongitude' => 9, 'gpsLatitude' => 10, 'flatNumber' => 11, 'createdAt' => 12, 'updatedAt' => 13, ),
        self::TYPE_COLNAME       => array(PropertyDetailsTableMap::COL_ID => 0, PropertyDetailsTableMap::COL_PROPERTY_ID => 1, PropertyDetailsTableMap::COL_SIZE => 2, PropertyDetailsTableMap::COL_NUMBER_OF_ROOM => 3, PropertyDetailsTableMap::COL_NUMBER_OF_FLOORS => 4, PropertyDetailsTableMap::COL_LOCATION => 5, PropertyDetailsTableMap::COL_PRICE => 6, PropertyDetailsTableMap::COL_STREET_ID => 7, PropertyDetailsTableMap::COL_HOUSE_NUMBER => 8, PropertyDetailsTableMap::COL_GPS_LONGITUDE => 9, PropertyDetailsTableMap::COL_GPS_LATITUDE => 10, PropertyDetailsTableMap::COL_FLAT_NUMBER => 11, PropertyDetailsTableMap::COL_CREATED_AT => 12, PropertyDetailsTableMap::COL_UPDATED_AT => 13, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'property_id' => 1, 'size' => 2, 'number_of_room' => 3, 'number_of_floors' => 4, 'location' => 5, 'price' => 6, 'street_id' => 7, 'house_number' => 8, 'gps_longitude' => 9, 'gps_latitude' => 10, 'flat_number' => 11, 'created_at' => 12, 'updated_at' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('property_details');
        $this->setPhpName('PropertyDetails');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\SystemBundle\\Model\\PropertyDetails');
        $this->setPackage('src\SystemBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('property_id', 'PropertyDetailsId', 'INTEGER', 'property', 'id', false, null, null);
        $this->addColumn('size', 'Size', 'FLOAT', false, null, null);
        $this->addColumn('number_of_room', 'NumberOfRoom', 'INTEGER', false, null, null);
        $this->addColumn('number_of_floors', 'NumberOfFloors', 'INTEGER', false, null, null);
        $this->addColumn('location', 'Location', 'VARCHAR', false, 255, null);
        $this->addColumn('price', 'Price', 'FLOAT', false, null, null);
        $this->addForeignKey('street_id', 'StreetId', 'INTEGER', 'street_list', 'id', false, null, null);
        $this->addColumn('house_number', 'HouseNumber', 'VARCHAR', false, 100, null);
        $this->addColumn('gps_longitude', 'GpsLongitude', 'VARCHAR', false, 100, null);
        $this->addColumn('gps_latitude', 'GpsLatitude', 'VARCHAR', false, 100, null);
        $this->addColumn('flat_number', 'FlatNumber', 'VARCHAR', false, 100, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Property', '\\SystemBundle\\Model\\Property', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':property_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('StreetList', '\\SystemBundle\\Model\\StreetList', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':street_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PropertyDetailsTableMap::CLASS_DEFAULT : PropertyDetailsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PropertyDetails object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PropertyDetailsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PropertyDetailsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PropertyDetailsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PropertyDetailsTableMap::OM_CLASS;
            /** @var PropertyDetails $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PropertyDetailsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PropertyDetailsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PropertyDetailsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PropertyDetails $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PropertyDetailsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_ID);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_PROPERTY_ID);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_SIZE);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_NUMBER_OF_ROOM);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_NUMBER_OF_FLOORS);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_LOCATION);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_PRICE);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_STREET_ID);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_HOUSE_NUMBER);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_GPS_LONGITUDE);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_GPS_LATITUDE);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_FLAT_NUMBER);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PropertyDetailsTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.property_id');
            $criteria->addSelectColumn($alias . '.size');
            $criteria->addSelectColumn($alias . '.number_of_room');
            $criteria->addSelectColumn($alias . '.number_of_floors');
            $criteria->addSelectColumn($alias . '.location');
            $criteria->addSelectColumn($alias . '.price');
            $criteria->addSelectColumn($alias . '.street_id');
            $criteria->addSelectColumn($alias . '.house_number');
            $criteria->addSelectColumn($alias . '.gps_longitude');
            $criteria->addSelectColumn($alias . '.gps_latitude');
            $criteria->addSelectColumn($alias . '.flat_number');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PropertyDetailsTableMap::DATABASE_NAME)->getTable(PropertyDetailsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PropertyDetailsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PropertyDetailsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PropertyDetailsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PropertyDetails or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PropertyDetails object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyDetailsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \SystemBundle\Model\PropertyDetails) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PropertyDetailsTableMap::DATABASE_NAME);
            $criteria->add(PropertyDetailsTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PropertyDetailsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PropertyDetailsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PropertyDetailsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the property_details table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PropertyDetailsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PropertyDetails or Criteria object.
     *
     * @param mixed               $criteria Criteria or PropertyDetails object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyDetailsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PropertyDetails object
        }

        if ($criteria->containsKey(PropertyDetailsTableMap::COL_ID) && $criteria->keyContainsValue(PropertyDetailsTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PropertyDetailsTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PropertyDetailsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PropertyDetailsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PropertyDetailsTableMap::buildTableMap();
