<?php

namespace HD\BenchmarkBundle\Services\Mailer;

use HD\BenchmarkBundle\Utils\EmailPreset\EmailPreset;
use Swift_Mailer;
use Twig_Environment;

class MailerService implements MailerInterface
{
    /** @var Twig_Environment */
    private $twig;
    /** @var Swift_Mailer */
    private $mailer;

    /**
     * MailerService constructor.
     * @param Twig_Environment $twig
     * @param Swift_Mailer $mailer
     */
    public function __construct(Twig_Environment $twig, Swift_Mailer $mailer)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    /**
     * @param EmailPreset $emailPreset
     * @throws \Exception
     */
    public function sendMail(EmailPreset $emailPreset)
    {
        try {
            $result = $this->createMail($emailPreset);
        }catch (\Exception $exception) {
            throw new \Exception('Some problem with sending email');
        }

        return $result;
    }

    /**
     * @param EmailPreset $emailPreset
     */
    private function createMail(EmailPreset $emailPreset)
    {
        $message = \Swift_Message::newInstance()
            ->setReplyTo($emailPreset->getReplayTo())
            ->setSubject($emailPreset->getSubject())
            ->setFrom($emailPreset->getFrom())
            ->setTo($emailPreset->getTo())
            ->setBcc($emailPreset->getBcc())
            ->setBody(
                $this->twig->render(
                    'HDBenchmarkBundle:Mail:information.html.twig',
                    [
                        'email' => $emailPreset->getEmail()
                    ]
                )
                , 'text/html');

        $result = $this->mailer->send($message);
    }
}