<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-07
 * Time: 15:45
 */

namespace Tests;

use PHPUnit_Framework_TestCase;
use Tests\Stubs\TestStub;

class PrivateAccessTestCaseTest extends PHPUnit_Framework_TestCase
{
    /** @var TestStub $testStub */
    private $testStub;
    /** @var PrivateAccessTestCase $privateAccessTestCase */
    private $privateAccessTestCase;
    public function setUp()
    {
        $this->testStub = new TestStub();
        $this->privateAccessTestCase = new PrivateAccessTestCase();
    }
    public function testGetPrivatePropertyValue()
    {
        /** @var int $testValue */
        $testValue = 123;
        $this->testStub->setPrivateProperty($testValue);
        $this->testStub->setProtectedProperty($testValue);
        $privatePropertyValue = $this->privateAccessTestCase->getPrivatePropertyValue($this->testStub, 'privateProperty');
        $protectedPropertyValue = $this->privateAccessTestCase->getPrivatePropertyValue($this->testStub, 'protectedProperty');
        $this->assertEquals($privatePropertyValue, $testValue);
        $this->assertEquals($protectedPropertyValue, $testValue);
    }
    /**
     * @expectedException \ReflectionException
     */
    public function testGetNotExistedPrivateProperty()
    {
        $this->privateAccessTestCase->getPrivatePropertyValue($this->testStub, 'dontExist');
    }
    public function testSetPrivatePropertyValue()
    {
        /** @var int $testValue */
        $testValue = 123;
        $this->privateAccessTestCase->setPrivatePropertyValue($this->testStub, 'privateProperty', $testValue);
        $this->privateAccessTestCase->setPrivatePropertyValue($this->testStub, 'protectedProperty', $testValue);
        $privatePropertyValue = $this->testStub->getPrivateProperty();
        $protectedPropertyValue = $this->testStub->getProtectedProperty();
        $this->assertEquals($privatePropertyValue, $testValue);
        $this->assertEquals($protectedPropertyValue, $testValue);
    }
    /**
     * @expectedException \ReflectionException
     */
    public function testSetNotExistedPrivatePropertyValue()
    {
        $this->privateAccessTestCase->setPrivatePropertyValue($this->testStub, 'dontExist', 'something');
    }
    public function testExecutePrivateMethod()
    {
        /** @var int $oldValue */
        $oldValue = 123;
        /** @var int $newValue */
        $newValue = 321;
        $this->testStub->setPrivateProperty($oldValue);
        $this->testStub->setProtectedProperty($oldValue);
        $this->privateAccessTestCase->executePrivateMethod($this->testStub, 'privateSetter', [$newValue]);
        $this->privateAccessTestCase->executePrivateMethod($this->testStub, 'protectedSetter', [$newValue]);
        $privatePropertyValue = $this->testStub->getPrivateProperty();
        $protectedPropertyValue = $this->testStub->getProtectedProperty();
        $this->assertEquals($privatePropertyValue, $newValue);
        $this->assertEquals($protectedPropertyValue, $newValue);
    }
    /**
     * @expectedException \ReflectionException
     */
    public function testExecuteNotExistedPrivateMethod()
    {
        $this->privateAccessTestCase->setPrivatePropertyValue($this->testStub, 'dontExistMethod', [123]);
    }
}