<?php

namespace HD\BenchmarkBundle\Services\MessageCoordinator;

use HD\BenchmarkBundle\Services\DTO\Benchmark\DTOShowResult;

class CheckerIfSendMessage
{
    /** @var MessageCoordinatorFactory */
    private $messageCoordinatorFactory;

    /**
     * CheckerIfSendMessage constructor.
     * @param MessageCoordinatorFactory $messageCoordinatorFactory
     */
    public function __construct(MessageCoordinatorFactory $messageCoordinatorFactory)
    {
        $this->messageCoordinatorFactory = $messageCoordinatorFactory;
    }

    /**
     * @param DTOShowResult $dtoShowResult
     */
    public function checkIfSendMessage(DTOShowResult $dtoShowResult)
    {
        if ($dtoShowResult->difference > 0) {
            $messageCoordinator = $this->messageCoordinatorFactory->chooseNotifierMessage(MessageCoordinatorFactory::CHANNEL_EMAIL);
            $messageCoordinator->sendMessage();
        }

        if ($dtoShowResult->timeFirst * 2 <= $dtoShowResult->timeSecond) {
            $messageCoordinator = $this->messageCoordinatorFactory->chooseNotifierMessage(MessageCoordinatorFactory::CHANNEL_SMS);
            $messageCoordinator->sendMessage();
        }
    }
}