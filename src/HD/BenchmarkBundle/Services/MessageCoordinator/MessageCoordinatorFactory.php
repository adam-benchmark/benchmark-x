<?php

namespace HD\BenchmarkBundle\Services\MessageCoordinator;

class MessageCoordinatorFactory
{
    const CHANNEL_EMAIL = 1;
    const CHANNEL_SMS = 2;
    const CHANNEL_FACEBOOK = 3;
    const CHANNEL_TWITTER = 4;

    /**
     * @param $channel
     * @return NotifierMessageEmail|NotifierMessageSms
     */
    public function chooseNotifierMessage($channel)
    {
        switch($channel) {
            case (self::CHANNEL_EMAIL):

                return new NotifierMessageEmail();
                break;
            case (self::CHANNEL_SMS):

                return new NotifierMessageSms();
                break;
            default:

                return new NotifierMessageEmail();
                break;
        }

    }
}