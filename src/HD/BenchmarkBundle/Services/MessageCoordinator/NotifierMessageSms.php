<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-15
 * Time: 21:52
 */

namespace HD\BenchmarkBundle\Services\MessageCoordinator;


class NotifierMessageSms implements NotifierMessageInterface
{
    public function sendMessage()
    {
        return 'send sms';
    }
}