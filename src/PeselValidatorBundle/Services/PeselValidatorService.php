<?php

namespace PeselValidatorBundle\Services;


use HD\BenchmarkBundle\Services\DTO\PeselValidator\DTOPeselValidator;
use InvalidArgumentException;

class PeselValidatorService
{
    const PESEL_LENGTH = 11;

    /**
     * @var array
     */
    private $weights = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1];

    /**
     * @var int
     */
    private $genderDigit = 9;

    /**
     * @var array
     */
    private $errorMessages = [
        'invalidLength' => 'Nieprawidłowa długość numeru PESEL.',
        'invalidCharacters' => 'Numer PESEL może zawierać tylko cyfry.',
        'invalidChecksum' => 'Numer PESEL posiada niepoprawną sumę kontrolną.',
    ];

    /**
     * @param DTOPeselValidator $dtoPeselValidator
     */
    public function isValidate(DTOPeselValidator $dtoPeselValidator)
    {

        /** @var string $pesel */
        $pesel = (string)$dtoPeselValidator->pesel;

        $this->validateLength($pesel);
        $this->validateDigitsOnly($pesel);
        $this->validateChecksum($pesel);
    }

    /**
     * Get gender encoded in the number.
     *
     * @return int
     */
    public function getGender($pesel)
    {
        return $pesel[$this->genderDigit] % 2;
    }


    /**
     * @throws InvalidArgumentException when number has invalid length
     */
    private function validateLength($pesel)
    {
        if (strlen($pesel) !== self::PESEL_LENGTH) {
            throw new InvalidArgumentException($this->errorMessages['invalidLength']);
        }
    }


    /**
     * @throws InvalidArgumentException when number contains non-digits
     */
    private function validateDigitsOnly($pesel)
    {
        if (ctype_digit($pesel) === false) {
            throw new InvalidArgumentException($this->errorMessages['invalidCharacters']);
        }
    }

    /**
     * @throws InvalidArgumentException on invalid checksum
     */
    private function validateChecksum($pesel)
    {
        $digitArray = str_split($pesel);
        $checksum = array_reduce(array_keys($digitArray), function ($carry, $index) use ($digitArray) {
            return $carry + $this->weights[$index] * $digitArray[$index];
        });
        if ($checksum % 10 !== 0) {
            throw new InvalidArgumentException($this->errorMessages['invalidChecksum']);
        }
    }
}