<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-14
 * Time: 07:59
 */

namespace HD\BenchmarkBundle\Services\LogService;

use HD\BenchmarkBundle\Services\DTO\Benchmark\DTOShowResult;

interface LogServiceInterface
{
    public function appendInfoToLogger(DTOShowResult $dtoShowResult, $fileName);

}