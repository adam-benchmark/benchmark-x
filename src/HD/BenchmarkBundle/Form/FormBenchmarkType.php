<?php

namespace HD\BenchmarkBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class FormBenchmarkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('urlFirst', UrlType::class, [
            'label' => 'Url First',
            'required' => true,
        ]);
        $builder->add('urlSecond', UrlType::class, [
            'label' => 'Url Second',
            'required' => true,
        ]);
        $builder->add('comment', TextType::class, [
            'label' => 'Comment',
            'required' => false,
        ]);
        $builder
            ->add('submit', SubmitType::class, [
                'label' => 'Start it Now!',
                'attr' => [
                    'style' => 'margin-top: 10px',
                    'class' => 'btn btn-outline btn-xl js-scroll-trigger',
                ]

            ]);
    }
}