<?php

namespace SystemBundle\Model\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SystemBundle\Model\PropertyDetails as ChildPropertyDetails;
use SystemBundle\Model\PropertyDetailsQuery as ChildPropertyDetailsQuery;
use SystemBundle\Model\Map\PropertyDetailsTableMap;

/**
 * Base class that represents a query for the 'property_details' table.
 *
 *
 *
 * @method     ChildPropertyDetailsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPropertyDetailsQuery orderByPropertyDetailsId($order = Criteria::ASC) Order by the property_id column
 * @method     ChildPropertyDetailsQuery orderBySize($order = Criteria::ASC) Order by the size column
 * @method     ChildPropertyDetailsQuery orderByNumberOfRoom($order = Criteria::ASC) Order by the number_of_room column
 * @method     ChildPropertyDetailsQuery orderByNumberOfFloors($order = Criteria::ASC) Order by the number_of_floors column
 * @method     ChildPropertyDetailsQuery orderByLocation($order = Criteria::ASC) Order by the location column
 * @method     ChildPropertyDetailsQuery orderByPrice($order = Criteria::ASC) Order by the price column
 * @method     ChildPropertyDetailsQuery orderByStreetId($order = Criteria::ASC) Order by the street_id column
 * @method     ChildPropertyDetailsQuery orderByHouseNumber($order = Criteria::ASC) Order by the house_number column
 * @method     ChildPropertyDetailsQuery orderByGpsLongitude($order = Criteria::ASC) Order by the gps_longitude column
 * @method     ChildPropertyDetailsQuery orderByGpsLatitude($order = Criteria::ASC) Order by the gps_latitude column
 * @method     ChildPropertyDetailsQuery orderByFlatNumber($order = Criteria::ASC) Order by the flat_number column
 * @method     ChildPropertyDetailsQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPropertyDetailsQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPropertyDetailsQuery groupById() Group by the id column
 * @method     ChildPropertyDetailsQuery groupByPropertyDetailsId() Group by the property_id column
 * @method     ChildPropertyDetailsQuery groupBySize() Group by the size column
 * @method     ChildPropertyDetailsQuery groupByNumberOfRoom() Group by the number_of_room column
 * @method     ChildPropertyDetailsQuery groupByNumberOfFloors() Group by the number_of_floors column
 * @method     ChildPropertyDetailsQuery groupByLocation() Group by the location column
 * @method     ChildPropertyDetailsQuery groupByPrice() Group by the price column
 * @method     ChildPropertyDetailsQuery groupByStreetId() Group by the street_id column
 * @method     ChildPropertyDetailsQuery groupByHouseNumber() Group by the house_number column
 * @method     ChildPropertyDetailsQuery groupByGpsLongitude() Group by the gps_longitude column
 * @method     ChildPropertyDetailsQuery groupByGpsLatitude() Group by the gps_latitude column
 * @method     ChildPropertyDetailsQuery groupByFlatNumber() Group by the flat_number column
 * @method     ChildPropertyDetailsQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPropertyDetailsQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPropertyDetailsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPropertyDetailsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPropertyDetailsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPropertyDetailsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPropertyDetailsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPropertyDetailsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPropertyDetailsQuery leftJoinProperty($relationAlias = null) Adds a LEFT JOIN clause to the query using the Property relation
 * @method     ChildPropertyDetailsQuery rightJoinProperty($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Property relation
 * @method     ChildPropertyDetailsQuery innerJoinProperty($relationAlias = null) Adds a INNER JOIN clause to the query using the Property relation
 *
 * @method     ChildPropertyDetailsQuery joinWithProperty($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Property relation
 *
 * @method     ChildPropertyDetailsQuery leftJoinWithProperty() Adds a LEFT JOIN clause and with to the query using the Property relation
 * @method     ChildPropertyDetailsQuery rightJoinWithProperty() Adds a RIGHT JOIN clause and with to the query using the Property relation
 * @method     ChildPropertyDetailsQuery innerJoinWithProperty() Adds a INNER JOIN clause and with to the query using the Property relation
 *
 * @method     ChildPropertyDetailsQuery leftJoinStreetList($relationAlias = null) Adds a LEFT JOIN clause to the query using the StreetList relation
 * @method     ChildPropertyDetailsQuery rightJoinStreetList($relationAlias = null) Adds a RIGHT JOIN clause to the query using the StreetList relation
 * @method     ChildPropertyDetailsQuery innerJoinStreetList($relationAlias = null) Adds a INNER JOIN clause to the query using the StreetList relation
 *
 * @method     ChildPropertyDetailsQuery joinWithStreetList($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the StreetList relation
 *
 * @method     ChildPropertyDetailsQuery leftJoinWithStreetList() Adds a LEFT JOIN clause and with to the query using the StreetList relation
 * @method     ChildPropertyDetailsQuery rightJoinWithStreetList() Adds a RIGHT JOIN clause and with to the query using the StreetList relation
 * @method     ChildPropertyDetailsQuery innerJoinWithStreetList() Adds a INNER JOIN clause and with to the query using the StreetList relation
 *
 * @method     \SystemBundle\Model\PropertyQuery|\SystemBundle\Model\StreetListQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPropertyDetails findOne(ConnectionInterface $con = null) Return the first ChildPropertyDetails matching the query
 * @method     ChildPropertyDetails findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPropertyDetails matching the query, or a new ChildPropertyDetails object populated from the query conditions when no match is found
 *
 * @method     ChildPropertyDetails findOneById(int $id) Return the first ChildPropertyDetails filtered by the id column
 * @method     ChildPropertyDetails findOneByPropertyDetailsId(int $property_id) Return the first ChildPropertyDetails filtered by the property_id column
 * @method     ChildPropertyDetails findOneBySize(double $size) Return the first ChildPropertyDetails filtered by the size column
 * @method     ChildPropertyDetails findOneByNumberOfRoom(int $number_of_room) Return the first ChildPropertyDetails filtered by the number_of_room column
 * @method     ChildPropertyDetails findOneByNumberOfFloors(int $number_of_floors) Return the first ChildPropertyDetails filtered by the number_of_floors column
 * @method     ChildPropertyDetails findOneByLocation(string $location) Return the first ChildPropertyDetails filtered by the location column
 * @method     ChildPropertyDetails findOneByPrice(double $price) Return the first ChildPropertyDetails filtered by the price column
 * @method     ChildPropertyDetails findOneByStreetId(int $street_id) Return the first ChildPropertyDetails filtered by the street_id column
 * @method     ChildPropertyDetails findOneByHouseNumber(string $house_number) Return the first ChildPropertyDetails filtered by the house_number column
 * @method     ChildPropertyDetails findOneByGpsLongitude(string $gps_longitude) Return the first ChildPropertyDetails filtered by the gps_longitude column
 * @method     ChildPropertyDetails findOneByGpsLatitude(string $gps_latitude) Return the first ChildPropertyDetails filtered by the gps_latitude column
 * @method     ChildPropertyDetails findOneByFlatNumber(string $flat_number) Return the first ChildPropertyDetails filtered by the flat_number column
 * @method     ChildPropertyDetails findOneByCreatedAt(string $created_at) Return the first ChildPropertyDetails filtered by the created_at column
 * @method     ChildPropertyDetails findOneByUpdatedAt(string $updated_at) Return the first ChildPropertyDetails filtered by the updated_at column *

 * @method     ChildPropertyDetails requirePk($key, ConnectionInterface $con = null) Return the ChildPropertyDetails by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOne(ConnectionInterface $con = null) Return the first ChildPropertyDetails matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPropertyDetails requireOneById(int $id) Return the first ChildPropertyDetails filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByPropertyDetailsId(int $property_id) Return the first ChildPropertyDetails filtered by the property_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneBySize(double $size) Return the first ChildPropertyDetails filtered by the size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByNumberOfRoom(int $number_of_room) Return the first ChildPropertyDetails filtered by the number_of_room column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByNumberOfFloors(int $number_of_floors) Return the first ChildPropertyDetails filtered by the number_of_floors column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByLocation(string $location) Return the first ChildPropertyDetails filtered by the location column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByPrice(double $price) Return the first ChildPropertyDetails filtered by the price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByStreetId(int $street_id) Return the first ChildPropertyDetails filtered by the street_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByHouseNumber(string $house_number) Return the first ChildPropertyDetails filtered by the house_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByGpsLongitude(string $gps_longitude) Return the first ChildPropertyDetails filtered by the gps_longitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByGpsLatitude(string $gps_latitude) Return the first ChildPropertyDetails filtered by the gps_latitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByFlatNumber(string $flat_number) Return the first ChildPropertyDetails filtered by the flat_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByCreatedAt(string $created_at) Return the first ChildPropertyDetails filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPropertyDetails requireOneByUpdatedAt(string $updated_at) Return the first ChildPropertyDetails filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPropertyDetails[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPropertyDetails objects based on current ModelCriteria
 * @method     ChildPropertyDetails[]|ObjectCollection findById(int $id) Return ChildPropertyDetails objects filtered by the id column
 * @method     ChildPropertyDetails[]|ObjectCollection findByPropertyDetailsId(int $property_id) Return ChildPropertyDetails objects filtered by the property_id column
 * @method     ChildPropertyDetails[]|ObjectCollection findBySize(double $size) Return ChildPropertyDetails objects filtered by the size column
 * @method     ChildPropertyDetails[]|ObjectCollection findByNumberOfRoom(int $number_of_room) Return ChildPropertyDetails objects filtered by the number_of_room column
 * @method     ChildPropertyDetails[]|ObjectCollection findByNumberOfFloors(int $number_of_floors) Return ChildPropertyDetails objects filtered by the number_of_floors column
 * @method     ChildPropertyDetails[]|ObjectCollection findByLocation(string $location) Return ChildPropertyDetails objects filtered by the location column
 * @method     ChildPropertyDetails[]|ObjectCollection findByPrice(double $price) Return ChildPropertyDetails objects filtered by the price column
 * @method     ChildPropertyDetails[]|ObjectCollection findByStreetId(int $street_id) Return ChildPropertyDetails objects filtered by the street_id column
 * @method     ChildPropertyDetails[]|ObjectCollection findByHouseNumber(string $house_number) Return ChildPropertyDetails objects filtered by the house_number column
 * @method     ChildPropertyDetails[]|ObjectCollection findByGpsLongitude(string $gps_longitude) Return ChildPropertyDetails objects filtered by the gps_longitude column
 * @method     ChildPropertyDetails[]|ObjectCollection findByGpsLatitude(string $gps_latitude) Return ChildPropertyDetails objects filtered by the gps_latitude column
 * @method     ChildPropertyDetails[]|ObjectCollection findByFlatNumber(string $flat_number) Return ChildPropertyDetails objects filtered by the flat_number column
 * @method     ChildPropertyDetails[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPropertyDetails objects filtered by the created_at column
 * @method     ChildPropertyDetails[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPropertyDetails objects filtered by the updated_at column
 * @method     ChildPropertyDetails[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PropertyDetailsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SystemBundle\Model\Base\PropertyDetailsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SystemBundle\\Model\\PropertyDetails', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPropertyDetailsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPropertyDetailsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPropertyDetailsQuery) {
            return $criteria;
        }
        $query = new ChildPropertyDetailsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPropertyDetails|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PropertyDetailsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PropertyDetailsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPropertyDetails A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, property_id, size, number_of_room, number_of_floors, location, price, street_id, house_number, gps_longitude, gps_latitude, flat_number, created_at, updated_at FROM property_details WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPropertyDetails $obj */
            $obj = new ChildPropertyDetails();
            $obj->hydrate($row);
            PropertyDetailsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPropertyDetails|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the property_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPropertyDetailsId(1234); // WHERE property_id = 1234
     * $query->filterByPropertyDetailsId(array(12, 34)); // WHERE property_id IN (12, 34)
     * $query->filterByPropertyDetailsId(array('min' => 12)); // WHERE property_id > 12
     * </code>
     *
     * @see       filterByProperty()
     *
     * @param     mixed $propertyDetailsId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByPropertyDetailsId($propertyDetailsId = null, $comparison = null)
    {
        if (is_array($propertyDetailsId)) {
            $useMinMax = false;
            if (isset($propertyDetailsId['min'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_PROPERTY_ID, $propertyDetailsId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($propertyDetailsId['max'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_PROPERTY_ID, $propertyDetailsId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_PROPERTY_ID, $propertyDetailsId, $comparison);
    }

    /**
     * Filter the query on the size column
     *
     * Example usage:
     * <code>
     * $query->filterBySize(1234); // WHERE size = 1234
     * $query->filterBySize(array(12, 34)); // WHERE size IN (12, 34)
     * $query->filterBySize(array('min' => 12)); // WHERE size > 12
     * </code>
     *
     * @param     mixed $size The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterBySize($size = null, $comparison = null)
    {
        if (is_array($size)) {
            $useMinMax = false;
            if (isset($size['min'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_SIZE, $size['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($size['max'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_SIZE, $size['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_SIZE, $size, $comparison);
    }

    /**
     * Filter the query on the number_of_room column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberOfRoom(1234); // WHERE number_of_room = 1234
     * $query->filterByNumberOfRoom(array(12, 34)); // WHERE number_of_room IN (12, 34)
     * $query->filterByNumberOfRoom(array('min' => 12)); // WHERE number_of_room > 12
     * </code>
     *
     * @param     mixed $numberOfRoom The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByNumberOfRoom($numberOfRoom = null, $comparison = null)
    {
        if (is_array($numberOfRoom)) {
            $useMinMax = false;
            if (isset($numberOfRoom['min'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_NUMBER_OF_ROOM, $numberOfRoom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numberOfRoom['max'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_NUMBER_OF_ROOM, $numberOfRoom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_NUMBER_OF_ROOM, $numberOfRoom, $comparison);
    }

    /**
     * Filter the query on the number_of_floors column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberOfFloors(1234); // WHERE number_of_floors = 1234
     * $query->filterByNumberOfFloors(array(12, 34)); // WHERE number_of_floors IN (12, 34)
     * $query->filterByNumberOfFloors(array('min' => 12)); // WHERE number_of_floors > 12
     * </code>
     *
     * @param     mixed $numberOfFloors The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByNumberOfFloors($numberOfFloors = null, $comparison = null)
    {
        if (is_array($numberOfFloors)) {
            $useMinMax = false;
            if (isset($numberOfFloors['min'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_NUMBER_OF_FLOORS, $numberOfFloors['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numberOfFloors['max'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_NUMBER_OF_FLOORS, $numberOfFloors['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_NUMBER_OF_FLOORS, $numberOfFloors, $comparison);
    }

    /**
     * Filter the query on the location column
     *
     * Example usage:
     * <code>
     * $query->filterByLocation('fooValue');   // WHERE location = 'fooValue'
     * $query->filterByLocation('%fooValue%', Criteria::LIKE); // WHERE location LIKE '%fooValue%'
     * </code>
     *
     * @param     string $location The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByLocation($location = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($location)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_LOCATION, $location, $comparison);
    }

    /**
     * Filter the query on the price column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE price = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE price IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE price > 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_PRICE, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_PRICE, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_PRICE, $price, $comparison);
    }

    /**
     * Filter the query on the street_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStreetId(1234); // WHERE street_id = 1234
     * $query->filterByStreetId(array(12, 34)); // WHERE street_id IN (12, 34)
     * $query->filterByStreetId(array('min' => 12)); // WHERE street_id > 12
     * </code>
     *
     * @see       filterByStreetList()
     *
     * @param     mixed $streetId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByStreetId($streetId = null, $comparison = null)
    {
        if (is_array($streetId)) {
            $useMinMax = false;
            if (isset($streetId['min'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_STREET_ID, $streetId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($streetId['max'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_STREET_ID, $streetId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_STREET_ID, $streetId, $comparison);
    }

    /**
     * Filter the query on the house_number column
     *
     * Example usage:
     * <code>
     * $query->filterByHouseNumber('fooValue');   // WHERE house_number = 'fooValue'
     * $query->filterByHouseNumber('%fooValue%', Criteria::LIKE); // WHERE house_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $houseNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByHouseNumber($houseNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($houseNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_HOUSE_NUMBER, $houseNumber, $comparison);
    }

    /**
     * Filter the query on the gps_longitude column
     *
     * Example usage:
     * <code>
     * $query->filterByGpsLongitude('fooValue');   // WHERE gps_longitude = 'fooValue'
     * $query->filterByGpsLongitude('%fooValue%', Criteria::LIKE); // WHERE gps_longitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gpsLongitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByGpsLongitude($gpsLongitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gpsLongitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_GPS_LONGITUDE, $gpsLongitude, $comparison);
    }

    /**
     * Filter the query on the gps_latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByGpsLatitude('fooValue');   // WHERE gps_latitude = 'fooValue'
     * $query->filterByGpsLatitude('%fooValue%', Criteria::LIKE); // WHERE gps_latitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gpsLatitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByGpsLatitude($gpsLatitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gpsLatitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_GPS_LATITUDE, $gpsLatitude, $comparison);
    }

    /**
     * Filter the query on the flat_number column
     *
     * Example usage:
     * <code>
     * $query->filterByFlatNumber('fooValue');   // WHERE flat_number = 'fooValue'
     * $query->filterByFlatNumber('%fooValue%', Criteria::LIKE); // WHERE flat_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $flatNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByFlatNumber($flatNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($flatNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_FLAT_NUMBER, $flatNumber, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PropertyDetailsTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PropertyDetailsTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \SystemBundle\Model\Property object
     *
     * @param \SystemBundle\Model\Property|ObjectCollection $property The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByProperty($property, $comparison = null)
    {
        if ($property instanceof \SystemBundle\Model\Property) {
            return $this
                ->addUsingAlias(PropertyDetailsTableMap::COL_PROPERTY_ID, $property->getId(), $comparison);
        } elseif ($property instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PropertyDetailsTableMap::COL_PROPERTY_ID, $property->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProperty() only accepts arguments of type \SystemBundle\Model\Property or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Property relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function joinProperty($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Property');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Property');
        }

        return $this;
    }

    /**
     * Use the Property relation Property object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SystemBundle\Model\PropertyQuery A secondary query class using the current class as primary query
     */
    public function usePropertyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProperty($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Property', '\SystemBundle\Model\PropertyQuery');
    }

    /**
     * Filter the query by a related \SystemBundle\Model\StreetList object
     *
     * @param \SystemBundle\Model\StreetList|ObjectCollection $streetList The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function filterByStreetList($streetList, $comparison = null)
    {
        if ($streetList instanceof \SystemBundle\Model\StreetList) {
            return $this
                ->addUsingAlias(PropertyDetailsTableMap::COL_STREET_ID, $streetList->getId(), $comparison);
        } elseif ($streetList instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PropertyDetailsTableMap::COL_STREET_ID, $streetList->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStreetList() only accepts arguments of type \SystemBundle\Model\StreetList or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the StreetList relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function joinStreetList($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('StreetList');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'StreetList');
        }

        return $this;
    }

    /**
     * Use the StreetList relation StreetList object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SystemBundle\Model\StreetListQuery A secondary query class using the current class as primary query
     */
    public function useStreetListQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinStreetList($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'StreetList', '\SystemBundle\Model\StreetListQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPropertyDetails $propertyDetails Object to remove from the list of results
     *
     * @return $this|ChildPropertyDetailsQuery The current query, for fluid interface
     */
    public function prune($propertyDetails = null)
    {
        if ($propertyDetails) {
            $this->addUsingAlias(PropertyDetailsTableMap::COL_ID, $propertyDetails->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the property_details table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyDetailsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PropertyDetailsTableMap::clearInstancePool();
            PropertyDetailsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyDetailsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PropertyDetailsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PropertyDetailsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PropertyDetailsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PropertyDetailsQuery
