<?php

namespace SystemBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;
use SystemBundle\Model\Property as ChildProperty;
use SystemBundle\Model\PropertyDetailsQuery as ChildPropertyDetailsQuery;
use SystemBundle\Model\PropertyQuery as ChildPropertyQuery;
use SystemBundle\Model\StreetList as ChildStreetList;
use SystemBundle\Model\StreetListQuery as ChildStreetListQuery;
use SystemBundle\Model\Map\PropertyDetailsTableMap;

/**
 * Base class that represents a row from the 'property_details' table.
 *
 *
 *
 * @package    propel.generator.src\SystemBundle.Model.Base
 */
abstract class PropertyDetails implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\SystemBundle\\Model\\Map\\PropertyDetailsTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the property_id field.
     *
     * @var        int
     */
    protected $property_id;

    /**
     * The value for the size field.
     *
     * @var        double
     */
    protected $size;

    /**
     * The value for the number_of_room field.
     *
     * @var        int
     */
    protected $number_of_room;

    /**
     * The value for the number_of_floors field.
     *
     * @var        int
     */
    protected $number_of_floors;

    /**
     * The value for the location field.
     *
     * @var        string
     */
    protected $location;

    /**
     * The value for the price field.
     *
     * @var        double
     */
    protected $price;

    /**
     * The value for the street_id field.
     *
     * @var        int
     */
    protected $street_id;

    /**
     * The value for the house_number field.
     *
     * @var        string
     */
    protected $house_number;

    /**
     * The value for the gps_longitude field.
     *
     * @var        string
     */
    protected $gps_longitude;

    /**
     * The value for the gps_latitude field.
     *
     * @var        string
     */
    protected $gps_latitude;

    /**
     * The value for the flat_number field.
     *
     * @var        string
     */
    protected $flat_number;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildProperty
     */
    protected $aProperty;

    /**
     * @var        ChildStreetList
     */
    protected $aStreetList;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of SystemBundle\Model\Base\PropertyDetails object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>PropertyDetails</code> instance.  If
     * <code>obj</code> is an instance of <code>PropertyDetails</code>, delegates to
     * <code>equals(PropertyDetails)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|PropertyDetails The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [property_id] column value.
     *
     * @return int
     */
    public function getPropertyDetailsId()
    {
        return $this->property_id;
    }

    /**
     * Get the [size] column value.
     *
     * @return double
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Get the [number_of_room] column value.
     *
     * @return int
     */
    public function getNumberOfRoom()
    {
        return $this->number_of_room;
    }

    /**
     * Get the [number_of_floors] column value.
     *
     * @return int
     */
    public function getNumberOfFloors()
    {
        return $this->number_of_floors;
    }

    /**
     * Get the [location] column value.
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Get the [price] column value.
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get the [street_id] column value.
     *
     * @return int
     */
    public function getStreetId()
    {
        return $this->street_id;
    }

    /**
     * Get the [house_number] column value.
     *
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->house_number;
    }

    /**
     * Get the [gps_longitude] column value.
     *
     * @return string
     */
    public function getGpsLongitude()
    {
        return $this->gps_longitude;
    }

    /**
     * Get the [gps_latitude] column value.
     *
     * @return string
     */
    public function getGpsLatitude()
    {
        return $this->gps_latitude;
    }

    /**
     * Get the [flat_number] column value.
     *
     * @return string
     */
    public function getFlatNumber()
    {
        return $this->flat_number;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [property_id] column.
     *
     * @param int $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setPropertyDetailsId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->property_id !== $v) {
            $this->property_id = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_PROPERTY_ID] = true;
        }

        if ($this->aProperty !== null && $this->aProperty->getId() !== $v) {
            $this->aProperty = null;
        }

        return $this;
    } // setPropertyDetailsId()

    /**
     * Set the value of [size] column.
     *
     * @param double $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setSize($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->size !== $v) {
            $this->size = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_SIZE] = true;
        }

        return $this;
    } // setSize()

    /**
     * Set the value of [number_of_room] column.
     *
     * @param int $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setNumberOfRoom($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->number_of_room !== $v) {
            $this->number_of_room = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_NUMBER_OF_ROOM] = true;
        }

        return $this;
    } // setNumberOfRoom()

    /**
     * Set the value of [number_of_floors] column.
     *
     * @param int $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setNumberOfFloors($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->number_of_floors !== $v) {
            $this->number_of_floors = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_NUMBER_OF_FLOORS] = true;
        }

        return $this;
    } // setNumberOfFloors()

    /**
     * Set the value of [location] column.
     *
     * @param string $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setLocation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->location !== $v) {
            $this->location = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_LOCATION] = true;
        }

        return $this;
    } // setLocation()

    /**
     * Set the value of [price] column.
     *
     * @param double $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->price !== $v) {
            $this->price = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_PRICE] = true;
        }

        return $this;
    } // setPrice()

    /**
     * Set the value of [street_id] column.
     *
     * @param int $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setStreetId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->street_id !== $v) {
            $this->street_id = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_STREET_ID] = true;
        }

        if ($this->aStreetList !== null && $this->aStreetList->getId() !== $v) {
            $this->aStreetList = null;
        }

        return $this;
    } // setStreetId()

    /**
     * Set the value of [house_number] column.
     *
     * @param string $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setHouseNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->house_number !== $v) {
            $this->house_number = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_HOUSE_NUMBER] = true;
        }

        return $this;
    } // setHouseNumber()

    /**
     * Set the value of [gps_longitude] column.
     *
     * @param string $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setGpsLongitude($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gps_longitude !== $v) {
            $this->gps_longitude = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_GPS_LONGITUDE] = true;
        }

        return $this;
    } // setGpsLongitude()

    /**
     * Set the value of [gps_latitude] column.
     *
     * @param string $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setGpsLatitude($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gps_latitude !== $v) {
            $this->gps_latitude = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_GPS_LATITUDE] = true;
        }

        return $this;
    } // setGpsLatitude()

    /**
     * Set the value of [flat_number] column.
     *
     * @param string $v new value
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setFlatNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->flat_number !== $v) {
            $this->flat_number = $v;
            $this->modifiedColumns[PropertyDetailsTableMap::COL_FLAT_NUMBER] = true;
        }

        return $this;
    } // setFlatNumber()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PropertyDetailsTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PropertyDetailsTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PropertyDetailsTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PropertyDetailsTableMap::translateFieldName('PropertyDetailsId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->property_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PropertyDetailsTableMap::translateFieldName('Size', TableMap::TYPE_PHPNAME, $indexType)];
            $this->size = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PropertyDetailsTableMap::translateFieldName('NumberOfRoom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->number_of_room = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PropertyDetailsTableMap::translateFieldName('NumberOfFloors', TableMap::TYPE_PHPNAME, $indexType)];
            $this->number_of_floors = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PropertyDetailsTableMap::translateFieldName('Location', TableMap::TYPE_PHPNAME, $indexType)];
            $this->location = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PropertyDetailsTableMap::translateFieldName('Price', TableMap::TYPE_PHPNAME, $indexType)];
            $this->price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PropertyDetailsTableMap::translateFieldName('StreetId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->street_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PropertyDetailsTableMap::translateFieldName('HouseNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->house_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PropertyDetailsTableMap::translateFieldName('GpsLongitude', TableMap::TYPE_PHPNAME, $indexType)];
            $this->gps_longitude = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PropertyDetailsTableMap::translateFieldName('GpsLatitude', TableMap::TYPE_PHPNAME, $indexType)];
            $this->gps_latitude = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PropertyDetailsTableMap::translateFieldName('FlatNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->flat_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : PropertyDetailsTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : PropertyDetailsTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = PropertyDetailsTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\SystemBundle\\Model\\PropertyDetails'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aProperty !== null && $this->property_id !== $this->aProperty->getId()) {
            $this->aProperty = null;
        }
        if ($this->aStreetList !== null && $this->street_id !== $this->aStreetList->getId()) {
            $this->aStreetList = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PropertyDetailsTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPropertyDetailsQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aProperty = null;
            $this->aStreetList = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see PropertyDetails::setDeleted()
     * @see PropertyDetails::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyDetailsTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPropertyDetailsQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PropertyDetailsTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PropertyDetailsTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aProperty !== null) {
                if ($this->aProperty->isModified() || $this->aProperty->isNew()) {
                    $affectedRows += $this->aProperty->save($con);
                }
                $this->setProperty($this->aProperty);
            }

            if ($this->aStreetList !== null) {
                if ($this->aStreetList->isModified() || $this->aStreetList->isNew()) {
                    $affectedRows += $this->aStreetList->save($con);
                }
                $this->setStreetList($this->aStreetList);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PropertyDetailsTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PropertyDetailsTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_PROPERTY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'property_id';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_SIZE)) {
            $modifiedColumns[':p' . $index++]  = 'size';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_NUMBER_OF_ROOM)) {
            $modifiedColumns[':p' . $index++]  = 'number_of_room';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_NUMBER_OF_FLOORS)) {
            $modifiedColumns[':p' . $index++]  = 'number_of_floors';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_LOCATION)) {
            $modifiedColumns[':p' . $index++]  = 'location';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'price';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_STREET_ID)) {
            $modifiedColumns[':p' . $index++]  = 'street_id';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_HOUSE_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'house_number';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_GPS_LONGITUDE)) {
            $modifiedColumns[':p' . $index++]  = 'gps_longitude';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_GPS_LATITUDE)) {
            $modifiedColumns[':p' . $index++]  = 'gps_latitude';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_FLAT_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'flat_number';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO property_details (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'property_id':
                        $stmt->bindValue($identifier, $this->property_id, PDO::PARAM_INT);
                        break;
                    case 'size':
                        $stmt->bindValue($identifier, $this->size, PDO::PARAM_STR);
                        break;
                    case 'number_of_room':
                        $stmt->bindValue($identifier, $this->number_of_room, PDO::PARAM_INT);
                        break;
                    case 'number_of_floors':
                        $stmt->bindValue($identifier, $this->number_of_floors, PDO::PARAM_INT);
                        break;
                    case 'location':
                        $stmt->bindValue($identifier, $this->location, PDO::PARAM_STR);
                        break;
                    case 'price':
                        $stmt->bindValue($identifier, $this->price, PDO::PARAM_STR);
                        break;
                    case 'street_id':
                        $stmt->bindValue($identifier, $this->street_id, PDO::PARAM_INT);
                        break;
                    case 'house_number':
                        $stmt->bindValue($identifier, $this->house_number, PDO::PARAM_STR);
                        break;
                    case 'gps_longitude':
                        $stmt->bindValue($identifier, $this->gps_longitude, PDO::PARAM_STR);
                        break;
                    case 'gps_latitude':
                        $stmt->bindValue($identifier, $this->gps_latitude, PDO::PARAM_STR);
                        break;
                    case 'flat_number':
                        $stmt->bindValue($identifier, $this->flat_number, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PropertyDetailsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getPropertyDetailsId();
                break;
            case 2:
                return $this->getSize();
                break;
            case 3:
                return $this->getNumberOfRoom();
                break;
            case 4:
                return $this->getNumberOfFloors();
                break;
            case 5:
                return $this->getLocation();
                break;
            case 6:
                return $this->getPrice();
                break;
            case 7:
                return $this->getStreetId();
                break;
            case 8:
                return $this->getHouseNumber();
                break;
            case 9:
                return $this->getGpsLongitude();
                break;
            case 10:
                return $this->getGpsLatitude();
                break;
            case 11:
                return $this->getFlatNumber();
                break;
            case 12:
                return $this->getCreatedAt();
                break;
            case 13:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['PropertyDetails'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['PropertyDetails'][$this->hashCode()] = true;
        $keys = PropertyDetailsTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getPropertyDetailsId(),
            $keys[2] => $this->getSize(),
            $keys[3] => $this->getNumberOfRoom(),
            $keys[4] => $this->getNumberOfFloors(),
            $keys[5] => $this->getLocation(),
            $keys[6] => $this->getPrice(),
            $keys[7] => $this->getStreetId(),
            $keys[8] => $this->getHouseNumber(),
            $keys[9] => $this->getGpsLongitude(),
            $keys[10] => $this->getGpsLatitude(),
            $keys[11] => $this->getFlatNumber(),
            $keys[12] => $this->getCreatedAt(),
            $keys[13] => $this->getUpdatedAt(),
        );
        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTimeInterface) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aProperty) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'property';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'property';
                        break;
                    default:
                        $key = 'Property';
                }

                $result[$key] = $this->aProperty->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aStreetList) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'streetList';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'street_list';
                        break;
                    default:
                        $key = 'StreetList';
                }

                $result[$key] = $this->aStreetList->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\SystemBundle\Model\PropertyDetails
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PropertyDetailsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\SystemBundle\Model\PropertyDetails
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setPropertyDetailsId($value);
                break;
            case 2:
                $this->setSize($value);
                break;
            case 3:
                $this->setNumberOfRoom($value);
                break;
            case 4:
                $this->setNumberOfFloors($value);
                break;
            case 5:
                $this->setLocation($value);
                break;
            case 6:
                $this->setPrice($value);
                break;
            case 7:
                $this->setStreetId($value);
                break;
            case 8:
                $this->setHouseNumber($value);
                break;
            case 9:
                $this->setGpsLongitude($value);
                break;
            case 10:
                $this->setGpsLatitude($value);
                break;
            case 11:
                $this->setFlatNumber($value);
                break;
            case 12:
                $this->setCreatedAt($value);
                break;
            case 13:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PropertyDetailsTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPropertyDetailsId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setSize($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNumberOfRoom($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setNumberOfFloors($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setLocation($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPrice($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setStreetId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setHouseNumber($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setGpsLongitude($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setGpsLatitude($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setFlatNumber($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCreatedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setUpdatedAt($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\SystemBundle\Model\PropertyDetails The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PropertyDetailsTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PropertyDetailsTableMap::COL_ID)) {
            $criteria->add(PropertyDetailsTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_PROPERTY_ID)) {
            $criteria->add(PropertyDetailsTableMap::COL_PROPERTY_ID, $this->property_id);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_SIZE)) {
            $criteria->add(PropertyDetailsTableMap::COL_SIZE, $this->size);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_NUMBER_OF_ROOM)) {
            $criteria->add(PropertyDetailsTableMap::COL_NUMBER_OF_ROOM, $this->number_of_room);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_NUMBER_OF_FLOORS)) {
            $criteria->add(PropertyDetailsTableMap::COL_NUMBER_OF_FLOORS, $this->number_of_floors);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_LOCATION)) {
            $criteria->add(PropertyDetailsTableMap::COL_LOCATION, $this->location);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_PRICE)) {
            $criteria->add(PropertyDetailsTableMap::COL_PRICE, $this->price);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_STREET_ID)) {
            $criteria->add(PropertyDetailsTableMap::COL_STREET_ID, $this->street_id);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_HOUSE_NUMBER)) {
            $criteria->add(PropertyDetailsTableMap::COL_HOUSE_NUMBER, $this->house_number);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_GPS_LONGITUDE)) {
            $criteria->add(PropertyDetailsTableMap::COL_GPS_LONGITUDE, $this->gps_longitude);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_GPS_LATITUDE)) {
            $criteria->add(PropertyDetailsTableMap::COL_GPS_LATITUDE, $this->gps_latitude);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_FLAT_NUMBER)) {
            $criteria->add(PropertyDetailsTableMap::COL_FLAT_NUMBER, $this->flat_number);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_CREATED_AT)) {
            $criteria->add(PropertyDetailsTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(PropertyDetailsTableMap::COL_UPDATED_AT)) {
            $criteria->add(PropertyDetailsTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPropertyDetailsQuery::create();
        $criteria->add(PropertyDetailsTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \SystemBundle\Model\PropertyDetails (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPropertyDetailsId($this->getPropertyDetailsId());
        $copyObj->setSize($this->getSize());
        $copyObj->setNumberOfRoom($this->getNumberOfRoom());
        $copyObj->setNumberOfFloors($this->getNumberOfFloors());
        $copyObj->setLocation($this->getLocation());
        $copyObj->setPrice($this->getPrice());
        $copyObj->setStreetId($this->getStreetId());
        $copyObj->setHouseNumber($this->getHouseNumber());
        $copyObj->setGpsLongitude($this->getGpsLongitude());
        $copyObj->setGpsLatitude($this->getGpsLatitude());
        $copyObj->setFlatNumber($this->getFlatNumber());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \SystemBundle\Model\PropertyDetails Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildProperty object.
     *
     * @param  ChildProperty $v
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     * @throws PropelException
     */
    public function setProperty(ChildProperty $v = null)
    {
        if ($v === null) {
            $this->setPropertyDetailsId(NULL);
        } else {
            $this->setPropertyDetailsId($v->getId());
        }

        $this->aProperty = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildProperty object, it will not be re-added.
        if ($v !== null) {
            $v->addPropertyDetails($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildProperty object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildProperty The associated ChildProperty object.
     * @throws PropelException
     */
    public function getProperty(ConnectionInterface $con = null)
    {
        if ($this->aProperty === null && ($this->property_id != 0)) {
            $this->aProperty = ChildPropertyQuery::create()->findPk($this->property_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aProperty->addPropertyDetailss($this);
             */
        }

        return $this->aProperty;
    }

    /**
     * Declares an association between this object and a ChildStreetList object.
     *
     * @param  ChildStreetList $v
     * @return $this|\SystemBundle\Model\PropertyDetails The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStreetList(ChildStreetList $v = null)
    {
        if ($v === null) {
            $this->setStreetId(NULL);
        } else {
            $this->setStreetId($v->getId());
        }

        $this->aStreetList = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildStreetList object, it will not be re-added.
        if ($v !== null) {
            $v->addPropertyDetails($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildStreetList object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildStreetList The associated ChildStreetList object.
     * @throws PropelException
     */
    public function getStreetList(ConnectionInterface $con = null)
    {
        if ($this->aStreetList === null && ($this->street_id != 0)) {
            $this->aStreetList = ChildStreetListQuery::create()->findPk($this->street_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStreetList->addPropertyDetailss($this);
             */
        }

        return $this->aStreetList;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aProperty) {
            $this->aProperty->removePropertyDetails($this);
        }
        if (null !== $this->aStreetList) {
            $this->aStreetList->removePropertyDetails($this);
        }
        $this->id = null;
        $this->property_id = null;
        $this->size = null;
        $this->number_of_room = null;
        $this->number_of_floors = null;
        $this->location = null;
        $this->price = null;
        $this->street_id = null;
        $this->house_number = null;
        $this->gps_longitude = null;
        $this->gps_latitude = null;
        $this->flat_number = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aProperty = null;
        $this->aStreetList = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PropertyDetailsTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
