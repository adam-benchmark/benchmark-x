<?php
namespace HD\BenchmarkBundle\Services\DTO\Benchmark;


class DTOBenchmark
{
    public $urlFirst;
    public $urlSecond;
    public $comment;
    public $date;
}