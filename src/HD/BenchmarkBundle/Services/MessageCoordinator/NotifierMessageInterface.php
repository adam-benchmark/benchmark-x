<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-15
 * Time: 22:04
 */

namespace HD\BenchmarkBundle\Services\MessageCoordinator;


interface NotifierMessageInterface
{
    public function sendMessage();
}