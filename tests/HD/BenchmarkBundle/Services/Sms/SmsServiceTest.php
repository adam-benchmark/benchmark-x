<?php

namespace HD\BenchmarkBundle\Services\Sms;

use Tests\PrivateAccessTestCase;

class SmsServiceTest extends PrivateAccessTestCase
{
    public function testSendSms()
    {
        $smsService = new SmsService();
        $result = $smsService->sendSms();
        $this->assertEquals('send sms', $result);
    }
}
