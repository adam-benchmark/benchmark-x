<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-15
 * Time: 20:34
 */

namespace HD\BenchmarkBundle\Utils\EmailPreset;


class EmailPreset
{
    private $subject;
    private $replayTo;
    private $from;
    private $to;
    private $bcc;
    private $email;

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getReplayTo()
    {
        return $this->replayTo;
    }

    /**
     * @param mixed $replayTo
     */
    public function setReplayTo($replayTo)
    {
        $this->replayTo = $replayTo;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return mixed
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @param mixed $bcc
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

}