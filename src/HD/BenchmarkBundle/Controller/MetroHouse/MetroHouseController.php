<?php

namespace HD\BenchmarkBundle\Controller\MetroHouse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MetroHouseController extends Controller
{
    public function indexAction(Request $request)
    {

        if ($request->isMethod('GET')) {
            //parse which page

        }



        return $this->render('@HDBenchmark/Metro/metro.html.twig', [
            'title' => 'metro'
        ]);
    }
}