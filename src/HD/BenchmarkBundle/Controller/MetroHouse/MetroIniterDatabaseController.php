<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-11-06
 * Time: 13:44
 */

namespace HD\BenchmarkBundle\Controller\MetroHouse;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MetroIniterDatabaseController extends Controller
{
    public function indexAction(Request $request)
    {
        if ($request->isMethod('GET')) {
            $option = $request->get('option');
        }

        $this->get('metro.database.init')->initialDatabase($option);

        return $this->render('@HDBenchmark/Metro/metro.html.twig', [
            'title' => 'metro initial'
        ]);
    }
}