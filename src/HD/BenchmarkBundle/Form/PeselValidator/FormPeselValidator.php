<?php

namespace HD\BenchmarkBundle\Form\PeselValidator;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FormPeselValidator extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pesel', TextType::class,[
                'label' => 'pesel',
                'required' => true
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Validate',
                'attr' => [
                    'style' => 'margin-top: 10px',
                    'class' => 'btn btn-outline btn-xl js-scroll-trigger',
                ]
            ])
            ;

    }
}