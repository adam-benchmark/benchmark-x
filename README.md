benchmark-website
=================

A Symfony project created on October 5, 2017, 8:37 pm.

<p>Subject:</p>
<h6>
App in PHP that can benchmark loading time of the website in comparison to the other
websites (check how fast is the website's loading time in comparison to other competitors).
</h6>

<p>Framework: <b>Symfony 3.3</b></p>
<p>Here You could see, how applications appears:<b>
/img/benchmark-snap.jpg</b>
</p>
<p>Logs are writes in:<b>/logs-benchmark/benchmark.log</b></p>
<p>100% services are tested</p>

<p>Code You can see here:</p>
<a href="https://bitbucket.org/adam-benchmark/benchmark-x/src">
https://bitbucket.org/adam-benchmark/benchmark-x/src
</a>