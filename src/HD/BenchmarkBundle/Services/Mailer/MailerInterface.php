<?php

namespace HD\BenchmarkBundle\Services\Mailer;

use HD\BenchmarkBundle\Utils\EmailPreset\EmailPreset;

/**
 * Interface MailerInterface
 * @package HD\BenchmarkBundle\Services\Mailer
 */
interface MailerInterface
{
    /**
     * @param EmailPreset $emailPreset
     * @return mixed
     */
    public function sendMail(EmailPreset $emailPreset);
}