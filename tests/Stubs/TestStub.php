<?php
/**
 * Created by PhpStorm.
 * User: adas
 * Date: 2017-10-07
 * Time: 15:47
 */

namespace Tests\Stubs;


class TestStub
{
    private $privateProperty;
    protected $protectedProperty;
    /**
     * @return mixed
     */
    public function getPrivateProperty()
    {
        return $this->privateProperty;
    }
    /**
     * @param mixed $privateProperty
     */
    public function setPrivateProperty($privateProperty)
    {
        $this->privateProperty = $privateProperty;
    }
    /**
     * @return mixed
     */
    public function getProtectedProperty()
    {
        return $this->protectedProperty;
    }
    /**
     * @param mixed $protectedProperty
     */
    public function setProtectedProperty($protectedProperty)
    {
        $this->protectedProperty = $protectedProperty;
    }
    /**
     * @param mixed $privateProperty
     */
    private function privateSetter($privateProperty)
    {
        $this->setPrivateProperty($privateProperty);
    }
    /**
     * @param mixed $protectedProperty
     */
    protected function protectedSetter($protectedProperty)
    {
        $this->setProtectedProperty($protectedProperty);
    }
}