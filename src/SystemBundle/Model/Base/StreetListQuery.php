<?php

namespace SystemBundle\Model\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use SystemBundle\Model\StreetList as ChildStreetList;
use SystemBundle\Model\StreetListQuery as ChildStreetListQuery;
use SystemBundle\Model\Map\StreetListTableMap;

/**
 * Base class that represents a query for the 'street_list' table.
 *
 *
 *
 * @method     ChildStreetListQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildStreetListQuery orderByStreet($order = Criteria::ASC) Order by the street column
 *
 * @method     ChildStreetListQuery groupById() Group by the id column
 * @method     ChildStreetListQuery groupByStreet() Group by the street column
 *
 * @method     ChildStreetListQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildStreetListQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildStreetListQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildStreetListQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildStreetListQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildStreetListQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildStreetListQuery leftJoinPropertyDetails($relationAlias = null) Adds a LEFT JOIN clause to the query using the PropertyDetails relation
 * @method     ChildStreetListQuery rightJoinPropertyDetails($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PropertyDetails relation
 * @method     ChildStreetListQuery innerJoinPropertyDetails($relationAlias = null) Adds a INNER JOIN clause to the query using the PropertyDetails relation
 *
 * @method     ChildStreetListQuery joinWithPropertyDetails($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PropertyDetails relation
 *
 * @method     ChildStreetListQuery leftJoinWithPropertyDetails() Adds a LEFT JOIN clause and with to the query using the PropertyDetails relation
 * @method     ChildStreetListQuery rightJoinWithPropertyDetails() Adds a RIGHT JOIN clause and with to the query using the PropertyDetails relation
 * @method     ChildStreetListQuery innerJoinWithPropertyDetails() Adds a INNER JOIN clause and with to the query using the PropertyDetails relation
 *
 * @method     \SystemBundle\Model\PropertyDetailsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildStreetList findOne(ConnectionInterface $con = null) Return the first ChildStreetList matching the query
 * @method     ChildStreetList findOneOrCreate(ConnectionInterface $con = null) Return the first ChildStreetList matching the query, or a new ChildStreetList object populated from the query conditions when no match is found
 *
 * @method     ChildStreetList findOneById(int $id) Return the first ChildStreetList filtered by the id column
 * @method     ChildStreetList findOneByStreet(string $street) Return the first ChildStreetList filtered by the street column *

 * @method     ChildStreetList requirePk($key, ConnectionInterface $con = null) Return the ChildStreetList by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreetList requireOne(ConnectionInterface $con = null) Return the first ChildStreetList matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStreetList requireOneById(int $id) Return the first ChildStreetList filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStreetList requireOneByStreet(string $street) Return the first ChildStreetList filtered by the street column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStreetList[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildStreetList objects based on current ModelCriteria
 * @method     ChildStreetList[]|ObjectCollection findById(int $id) Return ChildStreetList objects filtered by the id column
 * @method     ChildStreetList[]|ObjectCollection findByStreet(string $street) Return ChildStreetList objects filtered by the street column
 * @method     ChildStreetList[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class StreetListQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \SystemBundle\Model\Base\StreetListQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\SystemBundle\\Model\\StreetList', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildStreetListQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildStreetListQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildStreetListQuery) {
            return $criteria;
        }
        $query = new ChildStreetListQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildStreetList|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StreetListTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = StreetListTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStreetList A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, street FROM street_list WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildStreetList $obj */
            $obj = new ChildStreetList();
            $obj->hydrate($row);
            StreetListTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildStreetList|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildStreetListQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StreetListTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildStreetListQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StreetListTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetListQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(StreetListTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(StreetListTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetListTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the street column
     *
     * Example usage:
     * <code>
     * $query->filterByStreet('fooValue');   // WHERE street = 'fooValue'
     * $query->filterByStreet('%fooValue%', Criteria::LIKE); // WHERE street LIKE '%fooValue%'
     * </code>
     *
     * @param     string $street The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStreetListQuery The current query, for fluid interface
     */
    public function filterByStreet($street = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($street)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StreetListTableMap::COL_STREET, $street, $comparison);
    }

    /**
     * Filter the query by a related \SystemBundle\Model\PropertyDetails object
     *
     * @param \SystemBundle\Model\PropertyDetails|ObjectCollection $propertyDetails the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStreetListQuery The current query, for fluid interface
     */
    public function filterByPropertyDetails($propertyDetails, $comparison = null)
    {
        if ($propertyDetails instanceof \SystemBundle\Model\PropertyDetails) {
            return $this
                ->addUsingAlias(StreetListTableMap::COL_ID, $propertyDetails->getStreetId(), $comparison);
        } elseif ($propertyDetails instanceof ObjectCollection) {
            return $this
                ->usePropertyDetailsQuery()
                ->filterByPrimaryKeys($propertyDetails->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPropertyDetails() only accepts arguments of type \SystemBundle\Model\PropertyDetails or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PropertyDetails relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStreetListQuery The current query, for fluid interface
     */
    public function joinPropertyDetails($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PropertyDetails');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PropertyDetails');
        }

        return $this;
    }

    /**
     * Use the PropertyDetails relation PropertyDetails object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SystemBundle\Model\PropertyDetailsQuery A secondary query class using the current class as primary query
     */
    public function usePropertyDetailsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPropertyDetails($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PropertyDetails', '\SystemBundle\Model\PropertyDetailsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildStreetList $streetList Object to remove from the list of results
     *
     * @return $this|ChildStreetListQuery The current query, for fluid interface
     */
    public function prune($streetList = null)
    {
        if ($streetList) {
            $this->addUsingAlias(StreetListTableMap::COL_ID, $streetList->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the street_list table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreetListTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            StreetListTableMap::clearInstancePool();
            StreetListTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StreetListTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(StreetListTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            StreetListTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            StreetListTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // StreetListQuery
