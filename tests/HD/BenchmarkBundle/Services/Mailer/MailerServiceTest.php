<?php

namespace HD\BenchmarkBundle\Services\Mailer;

use Exception;
use Swift_Mailer;
use Tests\PrivateAccessTestCase;
use Twig_Environment;

class MailerServiceTest extends PrivateAccessTestCase
{
    /** @var  \Mockery */
    private $twig_Environment;
    /** @var  \Mockery */
    private $swift_Mailer;
    /** @var  MailerService */
    private $mailerService;

    public function setUp()
    {
        $this->twig_Environment = $this->createMock(Twig_Environment::class);
        $this->swift_Mailer = $this->createMock(Swift_Mailer::class);
        $this->mailerService = new MailerService($this->twig_Environment, $this->swift_Mailer);
    }

    public function testSendMail()
    {
        $dtoMailer = $this->createMock(DTOMailer::class);
        $result = $this->mailerService->sendMail($dtoMailer);
        $this->assertNull($result);
    }
}
